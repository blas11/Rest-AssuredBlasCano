  #language: es
    Característica: hacer Get de pet
    Yo como usuario de PetStore
    Quiero poder realizar un get de un  pet
    Para poder obtener información del pet
    @GET @PETSTORE
    Esquema del escenario: : buscar los siguientes pets por id y validar status
      Dado que como usuario de PetStore
      Cuando envio el id "<id>"
      Entonces debería devolverme información "<status>"
      Y debe validar el schema del Get

      Ejemplos:
      |id|status|
      |9| 200|
      |2| 200|
      |5| 200|
      |7| 200|
