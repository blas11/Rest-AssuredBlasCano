  #language: es
  Característica: agregar un nuevo pet
    Yo como usuario de PetStore
    Quiero poder agregar un nuevo pet
    Para poder tener registro del nuevo pet
    @POST @PETSTORE
    Esquema del escenario: agregar un nuevo pet y validar status
      Dado que como nuevo usuario de PetStore
      Cuando envio la información del pet "<nombre>"
      Entonces debería devolverme status "<status>"
      Y debe validar el schema del post

      Ejemplos:
        |nombre|status|
        |bobby| 200|
        |Michiu| 200|
        |Beto| 200|
        |Rumualda| 200|