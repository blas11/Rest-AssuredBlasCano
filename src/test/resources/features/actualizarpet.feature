  #language: es
  Característica: actualizar un pet
    Yo como usuario de PetStore
    Quiero poder actualizar la información de un pet
    Para poder tener un registro actual
    @PUT @PETSTORE
    Esquema del escenario: actualizar un  pet ,validar status y schema
      Dado que como usuario antiguo de PetStore
      Cuando ubico al pet por su id "<id>"
      Y envio información a actualizar "<name>"
      Entonces debería devolverme status del put "<status>"
      Y debe validar el schema del put

      Ejemplos:
        |id|name|status|
        |2|bobby| 200|
        |3|Michiu| 200|
        |5|Beto| 200|
        |7|Rumualda| 200|