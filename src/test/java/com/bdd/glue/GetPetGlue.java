package com.bdd.glue;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
public class GetPetGlue {

    private Response response;
    private String dominio;

    @Dado("que como usuario de PetStore")
    public void queComoUsuarioDePetStore (){
        dominio="https://petstore.swagger.io/v2/pet/";
    }
    @Cuando("envio el id {string}")
    public void envioElId(String id){
        response = RestAssured.get(dominio+id);
    }

    @Entonces("debería devolverme información {string}")
    public void deberiaDevolvermeInformacionDelPet(String sStatus){
        int iStatus = response.getStatusCode();
        Assertions.assertTrue(Integer.parseInt(sStatus)==iStatus);
    }
    @Y("debe validar el schema del Get")
    public void debeValidarElSchemaDelGet(){
        assertThat(response.getBody().asString(), matchesJsonSchemaInClasspath("schemas/petjsonschema.json"));
    }


}
