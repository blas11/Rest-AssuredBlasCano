package com.bdd.glue;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;

public class UpdatePetGlue {
    private Response response ;
    private String mostrarRequest;
    private String nombre;
    private String id;
    @BeforeAll
    public  static void setup(){
        //RestAssured.baseURI = "https://petstore.swagger.io/v2/pet";
    }

    @Dado("que como usuario antiguo de PetStore")
    public void queComoUsuarioDePetStore(){

    }
    @Cuando("ubico al pet por su id {string}")
    public  void ubicoAlPetPorSuId(String id ){
        //cambiar id por nombre
        this.id= id;

    }
    @Y("envio información a actualizar {string}")
    public void envioInformacionaActualizar(String name){


        nombre= '"'+name+'"';

        String requestBody = "{\"id\":"+ id+",\"category\":{\"id\":0,\"name\":\"string\"},\"name\":"+nombre+",\"photoUrls\":[\"string\"],\"tags\":[{\"id\":0,\"name\":\"string\"}],\"status\":\"available\"}";

        mostrarRequest=requestBody;

        response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .put("https://petstore.swagger.io/v2/pet")
                .then()
                .extract().response();
    }
    @Entonces("debería devolverme status del put {string}")
    public void deberiaDevolvermeInfomracion(String status){
        System.out.println(mostrarRequest);
        String responseBody = response.getBody().asString();
        Assertions.assertEquals(Integer.parseInt(status), response.statusCode());
    }
    @Y("debe validar el schema del put")
    public void debeValidarElSchemaDelPost(){
        assertThat(response.getBody().asString(), matchesJsonSchemaInClasspath("schemas/petjsonschema.json"));
    }





}
