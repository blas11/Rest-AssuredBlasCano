package com.bdd.glue;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddPetGlue {
    private Response response ;
    private String mostrarRequest;
    @BeforeAll
    public  static void setup(){
        //RestAssured.baseURI = "https://petstore.swagger.io/v2/pet";
    }

    @Dado("que como nuevo usuario de PetStore")
    public void queComoUsuarioDePetStore(){

    }
    @Cuando("envio la información del pet {string}")
    public  void envioLaInformacionDelPet(String nombre){
        nombre= '"'+nombre+'"';
      String requestBody = "{\"id\":0,\"category\":{\"id\":0,\"name\":\"string\"},\"name\":"+nombre+",\"photoUrls\":[\"string\"],\"tags\":[{\"id\":0,\"name\":\"string\"}],\"status\":\"available\"}";
    mostrarRequest=requestBody;



        response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .extract().response();
    }
    @Entonces("debería devolverme status {string}")
    public void deberiaDevolvermeInfomracion(String status){
        System.out.println(mostrarRequest);
        String responseBody = response.getBody().asString();
        Assertions.assertEquals(Integer.parseInt(status), response.statusCode());
    }
    @Y("debe validar el schema del post")
    public void debeValidarElSchemaDelPost(){
        assertThat(response.getBody().asString(), matchesJsonSchemaInClasspath("schemas/petjsonschema.json"));
    }


}
